import time

def product(p):
    count= 0
    result = 1
    for e in p:
        result = result * e
        count +=1
    return [result, count]

def find_match(p, target):
    count= 0
    for x in p:
        for y in p:
            count +=1
            if x * y == target:
                return [True, count]
    return [False, count]

def tricky_loop(p):
    count= 0
    while True:
        if len(p) == 0:
            break
        else:
            count +=1
            if p[-1] == 0:
                p.pop() # assume pop is a constant time operation
            else:
                p[-1] = 0
    return [101, count]

print [product(range(k))[-1]/k for k in range(100,1000,100)]
print [find_match(range(k),-1)[-1]/k for k in range(100,1000,100)]
print [tricky_loop(range(k))[-1]/k for k in range(100,1000,100)]
