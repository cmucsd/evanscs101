#Spelling Correction

#Double Gold Star

#For this question, your goal is to build a step towards a spelling corrector,
#similarly to the way Google used to respond,

#    "Did you mean: audacity"


#when you searched for "udacity" (but now considers "udacity" a real word!).

#One way to do spelling correction is to measure the edit distance between the
#entered word and other words in the dictionary.  Edit distance is a measure of
#the number of edits required to transform one word into another word.  An edit
#is either: (a) replacing one letter with a different letter, (b) removing a
#letter, or (c) inserting a letter.  The edit distance between two strings s and
#t, is the minimum number of edits needed to transform s into t.

#Define a procedure, edit_distance(s, t), that takes two strings as its inputs,
#and returns a number giving the edit distance between those strings.

#Note: it is okay if your edit_distance procedure is very expensive, and does
#not work on strings longer than the ones shown here.

#The built-in python function min() returns the mininum of all its arguments.

#print min(1,2,3)
#>>> 1

# see http://en.wikipedia.org/wiki/Damerau-Levenshtein_distance for the algorithm

def edit_distance(s,t):
    ls= len(s)+1
    lt= len(t)+1
    d=[[0 for tt in range(lt)] for ss in range(ls)]
    for ss in range(ls):
        d[ss][0]= ss
    for tt in range(1,lt):
        d[0][tt]= tt 
    for ss in range(1,ls):
        for tt in range(1,lt):
            if s[ss-1] == t[tt-1]:
                cost= 0
            else:
                cost= 1
            d[ss][tt]= min(d[ss-1][tt]+1, d[ss][tt-1]+1, d[ss-1][tt-1]+cost)
    return d[ls-1][lt-1]
#For example:

# Delete the 'a'
#print edit_distance('audacity', 'udacity')
#>>> 1

# Delete the 'a', replace the 'u' with 'U'
#print edit_distance('audacity', 'Udacity')
#>>> 2

# Five replacements
#print edit_distance('peter', 'sarah')
#>>> 5

# One deletion
#print edit_distance('pete', 'peter')
#>>> 1

#print edit_distance('Ei verbibbsch!', 'Ey ferbibsch!')
