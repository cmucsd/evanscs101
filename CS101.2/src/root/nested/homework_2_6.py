# Define a procedure, find_last, that takes as input
# two strings, a search string and a target string,
# and outputs the last position in the search string
# where the target string appears, or -1 if there
# are no occurences.
#
# Example: find_last('aaaa', 'a') returns 3

# Make sure your procedure has a return statement.

def find_last(st, pat):
    l= 1 #len(pat)
    pos= st.find(pat)
    p= pos
    while pos != -1:
        p= pos
        pos= st.find(pat, p+l)
    return p

#print find_last('ooga ooga ooga','og')
#print find_last('ooga ooga ooga','oynck')
#print find_last('boerp','boerp')
#print find_last('bbbbbbb','bb')
#print find_last('','')
#print find_last('','grunz')
#print find_last('grunz','')
#print find_last('aaaa','a')
