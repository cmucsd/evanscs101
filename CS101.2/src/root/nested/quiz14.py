#Define a procedure, abbaize, that takes
#two strings as its inputs, and returns
#a string that is the first input,
#followed by the first input.

#abbaize('a','b') => 'abba'
#abbaize('dog','cat') => 'dogcatcatdog'

def abbaize(oink, grunz):
    return oink+grunz+grunz+oink

print abbaize('a','b')
print abbaize('dog','cat')
