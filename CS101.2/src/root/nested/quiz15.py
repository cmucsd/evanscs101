#Define a procedure, find_second, that takes
#two strings as its inputs: a search string
#and a target string. It should output a
#number that is the position of the second
#occurence of the target string in the
#search string.

def find_second(nurnstuck, slotermeyer):
    pos= nurnstuck.find(slotermeyer)
    if -1 != pos:
        pos= nurnstuck.find(slotermeyer, pos+len(slotermeyer))
#       pos= nurnstuck.find(slotermeyer, pos+1)
    return pos
    
#print(find_second('aaaa','aa'))
#print(find_second('Helter Skelter','lter'))
#print(find_second('','.'))
#print(find_second('grunz',''))
#print(find_second('',''))