# Define a procedure, countdown, that takes a
# positive whole number as its input, and prints 
# out a countdown from that number to 1, 
# followed by Blastoff! 

def countdown(p):
    if p <= 0:
        print 'Blastoff!'
    else:
        print p
        countdown(p-1)

# countdown(7)
# countdown(2.71828)
# countdown(-3)
# countdown(0)
