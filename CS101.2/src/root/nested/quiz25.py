#Define a procedure, factorial, that
#takes one number as its input
#and returns the factorial of
#that number.

def factorial(n):
    def mul (foo,bar):
        return foo*bar
    return reduce(mul, range(1,n+1))

print factorial(5)
