# Define a procedure, median, that takes three
# numbers as its inputs, and outputs the median
# of the three numbers.

# Make sure your procedure has a return statement.

def bigger(a,b):
    if a > b:
        return a
    else:
        return b
        
def biggest(a,b,c):
    return bigger(a,bigger(b,c))

def median(a,b,c):
    if a > b:
        if b > c:
            m = b
        else:
            if a > c:
                m = c
            else:
                m = a
    else:
        if a > c:
            m = a
        else:
            if b > c:
                m = c
            else:
                m = b
    return m

# print [([i,j,k],median(i,j,k)) for i in range(3) for j in range(3) for k in range(3)]
