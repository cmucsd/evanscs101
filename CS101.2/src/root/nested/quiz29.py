# Modify the get_next_target procedure so that
# if there is a link it behaves as before, but
# if there is no link tag in the input string,
# it outputs None, 0.

def get_next_target(page):
    start_link = page.find('<a href=')

    #Insert your code here
    if 0 > start_link:
        return None, 0
    
    start_quote = page.find('"', start_link)
    end_quote = page.find('"', start_quote + 1)
    url = page[start_quote + 1:end_quote]
    return url, end_quote
                 

print get_next_target("oynck")
