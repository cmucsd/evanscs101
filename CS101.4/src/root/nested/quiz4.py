#Define a procedure, lookup,
#that takes two inputs:

#   - an index
#   - keyword

#The output should be a list
#of the urls associated
#with the keyword. If the keyword
#is not in the index, the output
#should be an empty list.

index = [['udacity', ['http://udacity.com', 'http://npr.org']], ['computing', ['http://acm.org']]]

def lookup(index,keyword):
    urls=[]
    for item in index:
        if keyword == item[0]:
            urls= item[1]
            break
    return urls

print lookup(index,'udacity') # ['http://udacity.com','http://npr.org']
print lookup(index,'computing')
print lookup(index,'schtonk')
