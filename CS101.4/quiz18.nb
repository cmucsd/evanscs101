(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      1538,         58]
NotebookOptionsPosition[      1196,         41]
NotebookOutlinePosition[      1535,         56]
CellTagsIndexPosition[      1492,         53]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<PhysicalConstants`\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.540932654918633*^9, 3.540932677194123*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 SuperscriptBox[
  RowBox[{"Convert", "[", 
   RowBox[{
    FractionBox[
     RowBox[{"4300.", " ", "Kilo", " ", "Meter"}], 
     RowBox[{"100", " ", "Milli", " ", "Second", " ", "SpeedOfLight"}]], ",", 
    "1"}], "]"}], 
  RowBox[{"-", "1"}]]], "Input",
 CellChangeTimes->{{3.540932688535078*^9, 3.540932760909886*^9}}],

Cell[BoxData["6.971917627906977`"], "Output",
 CellChangeTimes->{{3.540932744144759*^9, 3.540932761624736*^9}}]
}, Open  ]]
},
WindowSize->{740, 867},
WindowMargins->{{236, Automatic}, {-196, Automatic}},
FrontEndVersion->"8.0 for Linux x86 (32-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 149, 2, 30, "Input"],
Cell[CellGroupData[{
Cell[731, 26, 335, 9, 57, "Input"],
Cell[1069, 37, 111, 1, 30, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

