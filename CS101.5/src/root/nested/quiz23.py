#Creating an Empty Hash Table
#Define a procedure, make_hashtable,
#that takes as input a number, nbuckets,
#and outputs an empty hash table with
#nbuckets empty buckets.

def make_hashtable(nbuckets):
    return [[] for bucket in range(nbuckets)]

h= make_hashtable(4)

h[2]= ['TCH','Mary Joanna']

print h

    
