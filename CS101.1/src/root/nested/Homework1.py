#Write Python code that prints out the number of hours in 7 weeks.

#DO NOT USE IMPORT

hours_per_day=24
days_per_week=7

print hours_per_day*days_per_week*7

#Write Python code that stores the distance 
#in meters that light travels in one 
#nanosecond in the variable, nanodistance. 

#These variables are defined for you:
speed_of_light = 299800000. #meters per second
nano_per_sec = 1000000000. #1 Billion

#After your code,running
#print nanodistance
#should output 0.2998

#Note that nanodistance must be a decimal number.

#DO NOT USE IMPORT

nanodistance = speed_of_light / nano_per_sec




#ASSIGN nanodistance HERE

print nanodistance

#Given the variables s and t defined as:
s = 'udacity'
t = 'bodacious'
#write Python code that prints out udacious
#without using any quote characters in
#your code.

#DO NOT USE IMPORT

print s[:3]+t[4:]

#Assume text is a variable that
#holds a string. Write Python code
#that prints out the position
#of the first occurrence of 'hoo'
#in the value of text, or -1 if
#it does not occur at all.

text = "first hoo" 

#DO NOT USE IMPORT

#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

#fail = "Wachtmeister Dimpflmoser"
#ostfriesland = ''
#
#print fail.find('hoo')
#print ostfriesland.find('hoo')

print text.find('hoo')

#Assume text is a variable that
#holds a string. Write Python code
#that prints out the position
#of the second occurrence of 'zip'
#in text, or -1 if it does not occur
#at least twice.

#For example,
#   text = 'all zip files are zipped' -> 18
#   text = 'all zip files are compressed' -> -1

text = "all zip files are zipped" 

#DO NOT USE IMPORT

#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

fail= "all zip files are compressed"

def find_nth(logos, hae, n):
    pos= logos.find(hae)
    if (n>1) & (pos>-1):
        newpos=find_nth(logos[pos+len(hae):],hae,n-1)
        if newpos == -1:
            pos= newpos
        else:
            pos= pos+len(hae)+newpos
    return pos

#print find_nth(fail,'zip',1)
#print find_nth(fail,'zip',2)
print find_nth(text,'zip',2)
#print find_nth(text,'zip',3)
#print find_nth(text,'oynck',1)
#print find_nth(text,'oynck',2)
#print text[18:21]

#Given a variable, x, that stores
#the value of any decimal number,
#write Python code that prints out
#the nearest whole number to x.

#You can assume x is not negative.

# x = 3.14159 -> 3 (not 3.0)
# x = 27.63 -> 28 (not 28.0)

x = 3.14159

#DO NOT USE IMPORT

#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

def round_the_hard_way(r):
    s= int(r)
    if r-s >= 0.5:
        s=s+1
    return str(s)

#print round_the_hard_way(1.608)
print round_the_hard_way(x)
