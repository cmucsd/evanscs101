#Define a faster fibonacci procedure that will enable us to computer
#fibonacci(36).

def fibonacci(n):
    if n < 2:
        return n
    else:
        [f1, f0]= [1, 0]
        for k in range(1,n):
            [f1, f0]= [f1+f0, f1]
        return f1

print fibonacci(36)
#>>> 14930352