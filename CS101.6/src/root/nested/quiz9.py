#Define a procedure, factorial, that takes a natural number as its input, and
#outputs the number of ways to arrange the input number of itmes.

def factorial(n):
    if n<2:
        fac= 1
    else:
        fac= n*factorial(n-1)
    return fac



print factorial(0)
#>>> 1
print factorial(5)
#>>> 120
print factorial(10)
#>>> 3628800