# Congratulations! You have completed all of the content in
# the course. Amazing work. 
#
# If you have any message for us, feel free to write a print
# statement below and submit it. 
#
# Good luck with the homework and the final exam!
  
class RealProgrammerError(Exception):
    def __init__(self):
        self.value = 'Invite Peter to some tasty Quiche'
    def __str__(self):
        return repr(self.value)

def ask_Peter_about(topic):
    if 'FORTRAN' in topic.upper():
        raise RealProgrammerError()
    else:
        print 'Peter answers all questions, including yours about '+topic
    
ask_Peter_about('robots')
ask_Peter_about('Life, the Universe, and Everything')
ask_Peter_about('fortran stop command')