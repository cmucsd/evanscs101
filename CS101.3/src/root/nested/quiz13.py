#Define a procedure, replace_spy,
#that takes as its input a list of
#three numbers, and modifies the
#value of the third element in the
#input list to be one more than its
#previous value.

def replace_spy(list):
    list[-1]+=1

spy = [0,0,7]

replace_spy(spy)
print spy #=> [0,0,8]

