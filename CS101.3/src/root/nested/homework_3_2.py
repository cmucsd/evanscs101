def proc1(p):
    p[0]=p[1]

def proc2(p):
    p=p+[1]

def proc3(p):
    q=p
    p.append(3)
    q.pop()

def proc4(p):
    q=[]
    while p:
        q.append(p.pop())
    while q:
        p.append(q.pop())

testcase=['Strunz','dumm',5]
proc1(testcase)
print testcase

testcase=['Strunz','dumm',5]
proc2(testcase)
print testcase

testcase=['Strunz','dumm',5]
proc3(testcase)
print testcase

testcase=['Strunz','dumm',5]
proc4(testcase)
print testcase
