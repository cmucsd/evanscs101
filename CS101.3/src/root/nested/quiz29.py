#Define a procedure, union,
#that takes as inputs two lists.
#It should modify the first input
#list to be the set union of the two
#lists.
def union(l,m):
    for e in m:
        if e not in l:
            l.append(e)
    return l
a = [1,2,3]
b = [2,4,6]
union(a,b)
print a # => [1,2,3,4,6]
print b # => [2,4,6]
