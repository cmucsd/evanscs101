#Define a procedure, sum_list,
#that takes as its input a
#List of numbers, and produces
#as its output the sum of all
#the elements in the input list.
def sum_list(list):
    return reduce(lambda s,n : s+n, list)
#For example,
print sum_list([1,7,4]) # => 12
