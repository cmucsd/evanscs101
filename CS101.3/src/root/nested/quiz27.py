#Define a procedure, find_element,
#using index that takes as its
#inputs a List and a value of any
#type, and outputs the index of
#the first element in the input
#list that matches the value.

def find_element(list, element):
    if element in list:
        return list.index(element)
    else:
        return -1

#If there is no matching element,
#output -1.

print find_element([1,2,3],3) # => 2

print find_element(['alpha','beta'],'gamma') # => -1

