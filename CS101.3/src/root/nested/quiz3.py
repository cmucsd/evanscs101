#Given the variable,

days_in_month = [31,28,31,30,31,30,31,31,30,31,30,31]

#define a procedure, how_many_days,
#that takes as input a number
#representing a month, and outputs
#the number of days in that month.

def how_many_days(month):
    m= int(month-1)
    return days_in_month[m]

#how_many_days(1) => 31
#how_many_days(9) => 30
print how_many_days(5)
print [how_many_days(m) for m in range(12,0,-1)]
