#Define a procedure, measure_udacity,
#that takes its input a list of Strings,
#and outputs a number that is a count
#of the number of elements in the input
#list that start with the letter 'U'
#(uppercase).

def measure_udacity(list):
    return reduce(lambda s,e: s+e,[1 for st in list if st[0]=='U'],0)

#For example,

print measure_udacity(['Dave','Sebastian','Katy']) # => 0

print measure_udacity(['Umika','Umberto']) # => 2


